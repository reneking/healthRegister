﻿### 介绍
企业校园返工返校进出登记，全民战疫，包括taro前端 + 云开发 Serverless 函数 + 云数据库 + vue管理平台 + nodejs接口

### 项目简介
本项目的目标是为高校企业提供疫情期间进出登记的解决方案。

![输入图片说明](https://images.gitee.com/uploads/images/2020/0318/110615_37a767f4_5027575.png "微信图片_20200318110620.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/0318/111310_628529dd_5027575.jpeg "1584501180(1).jpg")
### 软件架构
基于 云+端 的架构模式开发

- 前端：Taro + TypeScript
- 后端：小程序·云开发 
- 数据层：云数据库
- 管理端：vue+nodejs


### 安装 & 预览

请提前安装好 TypeScript 依赖，版本大于等于 3.7。

```bash
$ npm i typescript -g
```

前端：

```bash
$ cd client/ 
$ npm install
$ npm run dev:weapp
```

后端：

```bash
$ cd cloud/functions/faas
$ npm install
$ tsc -w
```


### BUG 反馈
直接提 issues 即可

#### LICENSE
本项目基于 MIT 的开源规范：[LICENSE](./LICENSE)
