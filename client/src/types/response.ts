// https://transform.tools/json-to-typescript
// 自动生成

export interface StatResponse {
  data: {
    count: number,
    openId: string
  }
  error: any
  message: string
  status: 0
}

//
export interface ReturnReportResponse {
  data: {
    healthCount: number,
    hothCount: number,
    otherCount: number,
    openId: string
  }
  error: any
  message: string
  status: 0
}

export interface BaiduMapResponse {
  status: number
  result: Result
}

interface Result {
  location: Location
  formatted_address: string
  business: string
  addressComponent: AddressComponent
  pois: any[]
  roads: any[]
  poiRegions: PoiRegion[]
  sematic_description: string
  cityCode: number
}

interface Location {
  lng: number
  lat: number
}

interface AddressComponent {
  country: string
  country_code: number
  country_code_iso: string
  country_code_iso2: string
  province: string
  city: string
  city_level: number
  district: string
  town: string
  town_code: string
  adcode: string
  street: string
  street_number: string
  direction: string
  distance: string
}

interface PoiRegion {
  direction_desc: string
  name: string
  tag: string
  uid: string
  distance: string
}
