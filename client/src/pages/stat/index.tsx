import { Image, Text, View } from '@tarojs/components'
import { Config, useDidShow, useState } from '@tarojs/taro'
import { StatResponse } from 'src/types/response'
import { CounterContainer } from './components/CountContainer/CountContainer'
import './index.less'
import Icon from '../../assets/card-tip-icon.png'
import StatBG from '../../assets/统计-bg.png'

export function Stat() {
  const [total, setTotal] = useState(0)
  const [hotTotal, setHotTotal] = useState(0)
  const [closeTotal, setCloseTotal] = useState(0)
  // const [org_secrt,setOrg_secrt] = useState('')
  let org_secrt=""
  useDidShow(async () => {
    if (process.env.TARO_ENV == 'weapp') {
      wx.getStorage({
        key: 'org_secrt',
        success: (res: any) => {
          org_secrt=res.data
          console.log("org_secrt为：" + org_secrt)
          const response = Taro.cloud.callFunction({
            name: 'faas',
            data: { method: 'stat_by_orgName', data: { org_secrt: org_secrt } },
            success: res => {
              console.log("结果为：" + res)
              setTotal(res.result.data.totalCount.total)
              setHotTotal(res.result.data.hotCount.total)
              setCloseTotal(res.result.data.closeCount.total)
            }
          })
        }
      })
    } else {
      // h5 和 alipay
      org_secrt=localStorage.getItem("org_secrt")
      console.log("local storage" + localStorage)
      const response = Taro.cloud.callFunction({
        name: 'faas',
        data: { method: 'stat_by_orgName', data: { org_secrt: org_secrt } },
        success: res => {
          console.log("结果为：" + res)
        }
      })
    }

    // setTotal(result.data.count)
  })

  const toReport = () => {
    Taro.navigateTo({
      url: '/pages/anonymous/index',
    })
  }

  const now = new Date()
  const before = new Date(new Date().setDate((new Date().getDate() - 14)))//14天前
  const date = `${before.getFullYear()}年${before.getMonth() + 1}月${before.getDate()}日` + '-' + `${now.getFullYear()}年${now.getMonth() + 1}月${now.getDate()}日`
  const hasData = true;

  // if (hasData) {
  return (
    <View className="container" >
      <Image src={StatBG} className="background" />
      <View className="summary">
        <Text className="date">{date}</Text>
        <Text className="report-count">共 {total} 人上报健康状态</Text>
        <CounterContainer healthCount={total-hotTotal} hotCount={hotTotal} closeCount={closeTotal} />
      </View>
      {/* <View className="tip-container">
          <Image className="tip-icon" src={Icon} />
          <Text className="title">返校病患同行</Text>
          <Text onClick={toReport} className="report">
            信息上报
        </Text>
        </View>
        <View className="info-container">
          <Text className="info">
            共有 <Text className="num">32</Text> 人曾与确诊病患同行
        </Text>
          <View className="line" />
          <Text className="today">
            今日 <Text className="today-count">+{total}</Text> 人
        </Text>
        </View>
        <View className="tip-container">
          <Image className="tip-icon" src={Icon} />
          <Text className="title">校园疫情地图</Text>
        </View>
        <View className="building-container">
          <Text className="building">主教学楼</Text>
          <Text className="building-info">
            至今已有 <Text style={{ color: '#2969ee' }}>54</Text> 人去过，<Text style={{ color: '#ff0000' }}>1</Text>{' '}
            人曾与确诊病患同行
        </Text>
        </View>
        <View className="progress" />
        <View className="building-container">
          <Text className="building">艺术学院</Text>
          <Text className="building-info">
            至今已有 <Text style={{ color: '#2969ee' }}>54</Text> 人去过
        </Text>
        </View>
        <View className="progress" />
        <View className="building-container">
          <Text className="building">信息工程学院</Text>
          <Text className="building-info">
            至今已有 <Text style={{ color: '#2969ee' }}>54</Text> 人去过
        </Text>
        </View>
        <View className="progress" />
        <View className="patch" /> */}
    </View>
  )
  // }
  // else {
  //   return (
  //     <View className="container" >
  //       <Image src={StatBG} className="background" />
  //       <View className="summary">
  //         <Text className="date">{date}</Text>
  //   <Text className="report-count">共{totalCount}</Text>
  //         <CounterContainer />
  //       </View>
  //     </View>
  //   )
  // }
}

Stat.config = {
  navigationBarTitleText: '统计',
} as Config
