import { Image, Text, View, Form, Switch, Input, Button } from '@tarojs/components'
import { Config, useDidShow, useState } from '@tarojs/taro'
import { ReturnReportResponse } from 'src/types/response'
import './index.less'
import Icon from '../../assets/card-tip-icon.png'
import StatBG from '../../assets/统计-bg.png'
export function Stat() {
    const [total, setTotal] = useState(0)
    const [org_secrt, setOrg_secrt] = useState("")
    const [org_name, setOrg_name] = useState("")
    useDidShow(async () => {
        if (this.$router.params.org_secrt != "" && this.$router.params.org_secrt != undefined) {
            setOrg_secrt(this.$router.params.org_secrt)
        } else {
            setOrg_secrt("test0227");
        }
        if (this.$router.params.org_name != "" && this.$router.params.org_name != undefined) {
            setOrg_name(this.$router.params.org_name)
        } else {
            setOrg_name("测试机构");
        }
        // setTotal(result.data.count)
    })

    const toReport = () => {
        Taro.navigateTo({
            url: '/pages/stat/index',
        })
    }
    const formSubmit = async (e: any) => {
        console.log(e.detail.value)
        Taro.cloud.callFunction({
            name: 'faas',
            data: { method: 'return_report', data: e.detail.value },
            success: res => {
                console.log(res)
                Taro.showToast({ title: res.result.message, duration: 2000 })
                setTimeout(() => {
                    Taro.switchTab({url:'/pages/stat/index?org_secrt='+org_secrt})
                }, 1500);
            },
        })


        // await toReport()
    }

    const now = new Date()
    const date = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`
    const hasData = false;


    return (

        <View className="container" >
            <View className="colorBar"></View>
            <Form onSubmit={formSubmit} className="formbody">
                <View >
                    <Text className="title1">疫情返工/返校上报</Text>
                    <Text className="title2">请认真如实填写，抗击新冠，维护企业校园健康环境</Text>
                    <Text className="title">人员姓名(必填)</Text>
                    <Input name="personName" className="forminput" type='text' placeholder='' />
                    <Text className="title">所在单位/学校</Text>
                    <Input name="orgName" value={org_name} disabled className="forminput" type='text' placeholder='' />
                    <Text className="title">所在部门/院系班级</Text>
                    <Input name="deptName" className="forminput" type='text' placeholder='' />
                    <Text className="title">是否有发热状态(发热请勾选)</Text>
                    <Switch name='feswitch' className='form-switch'></Switch>
                    <Text className="title">是否接触来自湖北人员(接触请勾选)</Text>
                    <Switch name='jcswitch' className='form-switch'></Switch>
                    <Text className="title">手机</Text>
                    <Input name="tel" className="forminput" type='text' placeholder='' />
                    <Input name="orgSecrt" value={org_secrt} style="display:none;" className="forminput" type='text' placeholder='' />
                    <Button className='submitBtn' plain formType="submit">提交</Button>
                </View>
            </Form>

        </View>
    )



}

Stat.config = {
    navigationBarTitleText: '统计',
} as Config
