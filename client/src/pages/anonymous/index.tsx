import { View, Text } from '@tarojs/components'
import Taro, { Config, useEffect, useState, useDidShow, UserInfo } from '@tarojs/taro'
import { AtButton, AtTextarea } from 'taro-ui'
import './index.less'
import { BaiduMapResponse } from 'src/types/response'

export function Anonymous() {
  const [value, setValue] = useState('')
  const [loading, setLoading] = useState(false)
  const [signin_address, setSignin_address] = useState("")
  const [signin_time, setSignin_time] = useState("")
  const [userInfo, setUserInfo] = useState<UserInfo>({} as UserInfo)

 
  useDidShow(async () => {//生命周期钩子
    const params= this.$router.params
    if(params.scene!=1011){
      Taro.showModal({
        title: '温馨提示',
        content: '请扫描打卡二维码进入才能打卡哦',
        showCancel: false,
        success:(res)=>{
          if(res.confirm){
            Taro.switchTab({
              url:'/pages/index/index'
            });
            return;
          }
        }
      })
    }
  })
  const report = async () => {
    Taro.showLoading({
      title: '报告中',
    })

    setLoading(true)
    const { userInfo } = await Taro.getUserInfo()
    setUserInfo(userInfo)
    const location = await getLocation()
    setSignin_address(location)
    Taro.cloud.callFunction({
      name: 'faas',
      data: { method: 'anonymous_report', data: { value: value, signinaddress: location, personname: userInfo.nickName } },
      success: res => {
        console.log(res.result.data)

        setSignin_time(res.result.data.sign_date);
        Taro.hideLoading()
        Taro.showToast({
          title: '成功',
          icon: 'success',
          duration: 2000
        })

        setLoading(false)
      },


    })
    Taro.hideLoading()
    Taro.showToast({
      title: '成功',
      icon: 'success',
      duration: 2000
    })

    setLoading(false)

  }

  const getLocation = async () => {
    try {
      await Taro.authorize({
        scope: 'scope.userLocation',
      })

      const { latitude, longitude } = await Taro.getLocation({})

      const { data } = await Taro.request<BaiduMapResponse>({
        url: `https://api.map.baidu.com/geocoder/v2/?ak=lr7Cl7QkbGqH5irqlK5wZvwQ9duK84hc&location=${latitude},${longitude}&output=json`,
      })
      return data.result.addressComponent.province + data.result.addressComponent.city + data.result.addressComponent.district + data.result.addressComponent.street + data.result.addressComponent.street_number + data.result.addressComponent.direction

    } catch (e) { }
  }

  return (
    <View className="container">
      <View className="text">
        <AtTextarea
          className="att"
          value={value}
          onChange={(event) => setValue((event.target as any).value)}
          maxLength={200}
          height={Taro.pxTransform(379)}
          placeholder="如有特殊情况，例如发热、接触了感染人员等，请输入你想上报的信息。(选填)"
        />
        <View className="text-container">
          <Text className="title">打卡地点：</Text>
          <Text className="tip">{signin_address}</Text>
        </View>
        <View className="text-container">
          <Text className="title">打卡时间：</Text>
          <Text className="tip">{signin_time}</Text>
        </View>
      </View>

      <View className="btn">
        <AtButton onClick={report} disabled={loading} type="primary">打卡</AtButton>
      </View>
    </View>
  )
}

Anonymous.config = {
  navigationBarTitleText: '今日打卡',
} as Config
