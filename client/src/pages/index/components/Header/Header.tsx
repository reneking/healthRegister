import { Image, View, Text } from '@tarojs/components'
import './header.less'
import Background from '../../../../assets/首页背景.png'
import LocationIcon from '../../../../assets/定位.png'

type Props = {
  name: string
  avatar: string
  city: string
  org_name: string
}

export function Header(props: Props) {
  return (
    <View className="container">
      <Image src={Background} className="background" />
      <View className="user-container">
        <Image src={props.avatar} className="avatar" />
        <View>
          <Text className="name">{props.name} {props.org_name}</Text>
          {props.city && (
            <View className="location">
              <Image className="location-icon" src={LocationIcon} />
              <Text className="city">{props.city}</Text>
            </View>
          )}
        </View>
      </View>
    </View>
  )
}
