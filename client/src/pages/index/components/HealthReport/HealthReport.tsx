import { Button, Text, View } from '@tarojs/components'
import Taro from '@tarojs/taro'
import './style.less'
type Props = {
  org_secrt: string
  org_name: string
  isAuth: Boolean
  baidu_location:string
  scene:Number
}
export function HealthReport(props: Props) {
  const now = new Date()
  const date = `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`
  debugger
  //非接触式打卡
  const signIn = () => {
    Taro.navigateTo({
      url: '/pages/anonymous/index?&org_secrt=' + props.org_secrt+'&scene='+props.scene,
    })
  }
  if (!props.isAuth) {
    return (
      <View className="container">
        <Button disabled onClick={signIn} className="report-button">
          <View className="report-button-icon" />
          <Text>点击下方按钮登录</Text>
        </Button>
        <Text className="date">{date}</Text>
        <View className="text-container">
          <Text className="title">健康状态：</Text>
          <Text className="tip">记得上报健康情况哦</Text>
        </View>
        <View className="text-container">
          <Text className="title">行程排查：</Text>
          <Text className="tip">填写行程，后台将查询是否曾与确诊病患同行。有情况会第一时间通知并采取措施</Text>
        </View>
      </View>
    )
  }
  return (
    <View className="container">
      <Button onClick={signIn} className="report-button">
        <View className="report-button-icon" />
        <Text>非接触式打卡</Text>
      </Button>
      <Text className="date">{date}</Text>
      <View className="text-container">
        <Text className="title">健康状态：</Text>
        <Text className="tip">记得上报健康情况哦</Text>
      </View>
      <View className="text-container">
        <Text className="title">行程排查：</Text>
        <Text className="tip">填写行程，后台将查询是否曾与确诊病患同行。有情况会第一时间通知并采取措施</Text>
      </View>
    </View>

}
