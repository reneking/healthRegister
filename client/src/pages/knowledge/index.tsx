import { View } from '@tarojs/components'
import Taro, { Config, useDidShow } from '@tarojs/taro'
import './index.less'
import { Card } from '../../components/Card/Card'

type Link = {
  title: string
  desc: string
  path?: string
  appId?: string
}

const links: Link[] = [
  {
    title: '防疫小妙招',
    desc: '快来看看有什么防疫小妙招吧',
    path: 'pages/webview/index.html?url=https%253A%252F%252F3g.dxy.cn%252Fnewh5%252Fview%252Fpneumonia',
    appId: 'wxffc1051032845ffa'
  },
  {
    title: '健康咨询',
    desc: '在线咨询三甲医生',
    path: 'pages/home/home',
    appId: 'wx37c2f30fb79dbbe8'
  },
  {
    title: '腾讯课堂',
    desc: '在家也可以继续学习',
    appId: 'wxa2c453d902cdd452',
    path: 'pages/index/index.html'
  }
]

export function Knowledge() {
  useDidShow(async () => {//生命周期钩子
    const setting = await Taro.getSetting()
    // console.log(this.router.params)
    
  })
  const toMiniProgram = (appId: string, path: string) => {
    Taro.navigateToMiniProgram({
      appId,
      path
    })
  }

  return (
    <View className="container">
      {links.map((link) => (
        <Card onClick={() => toMiniProgram(link.appId, link.path)} key={link.title} title={link.title} desc={link.desc} />
      ))}
    </View>
  )
}

Knowledge.config = {
  navigationBarTitleText: '健康防疫知识合集',
} as Config
